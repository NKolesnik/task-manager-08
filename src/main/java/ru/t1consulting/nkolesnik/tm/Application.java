package ru.t1consulting.nkolesnik.tm;

import ru.t1consulting.nkolesnik.tm.api.ICommandRepository;
import ru.t1consulting.nkolesnik.tm.constant.ArgumentConst;
import ru.t1consulting.nkolesnik.tm.constant.TerminalConst;
import ru.t1consulting.nkolesnik.tm.model.Command;
import ru.t1consulting.nkolesnik.tm.repository.CommandRepository;
import ru.t1consulting.nkolesnik.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    public static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (processArgument(args)) {
            System.exit(0);
        }
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) {
            return;
        }
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) {
            return false;
        }
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void close() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikolay Kolesnik");
        System.out.println("E-mail: kolesnik.nik.vrn@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    private static void showCommands(){
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showArguments(){
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    public static void showSystemInfo() {
        System.out.println("[INFO]");
        final Runtime runtime = Runtime.getRuntime();
        final int processors = runtime.availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryOutput = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryOutput);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showErrorArgument(final String arg) {
        System.err.printf("Error! This argument `%s` is not supported\n", arg);
    }

    public static void showErrorCommand(final String arg) {
        System.err.printf("Error! This command `%s` is not supported\n", arg);
    }

}
